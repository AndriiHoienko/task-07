package com.epam.rd.java.basic.task7.db;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;


public class DBManager {

    private static DBManager instance;

    public static synchronized DBManager getInstance() {
        if (instance == null) {
            instance = new DBManager();
        }
        return instance;
    }

    private Connection getConnection() throws SQLException {
        String url = "jdbc:derby:memory:testdb;create=true";

        return DriverManager.getConnection(url);
    }


    private DBManager() {
    }

    public List<User> findAllUsers() throws DBException {
        List<User> users = new ArrayList<>();
        try (Connection con = getConnection();
             Statement statement = con.createStatement();
             ResultSet set = statement.executeQuery("SELECT * FROM users")) {
            while (set.next()) {
                User user = new User();
                user.setId(set.getInt("id"));
                user.setLogin(set.getString("login"));
                users.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return users;
    }

    public boolean insertUser(User user) throws DBException {
        String sql = "INSERT INTO users (login) VALUES (?)";
        try (Connection con = getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, user.getLogin());
            statement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean deleteUsers(User... users) throws DBException {
        String sql = "DELETE FROM users WHERE id = ? AND login = ?";
        try (Connection con = getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            for (User user: users) {
                statement.setInt(1, user.getId());
                statement.setString(2, user.getLogin());
            }
            statement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public User getUser(String login) throws DBException {
        String sql = "SELECT * FROM users WHERE login = ?";
        try (Connection con = getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
				statement.setString(1, login);
				ResultSet userData = statement.executeQuery();
				userData.next();
				User user = new User();
				user.setId(userData.getInt("id"));
				user.setLogin(userData.getString("login"));
				return user;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Team getTeam(String name) throws DBException {
		String sql = "SELECT * FROM teams WHERE name = ?";
		try (Connection con = getConnection();
			 PreparedStatement statement = con.prepareStatement(sql)) {
			statement.setString(1, name);
			ResultSet teamData = statement.executeQuery();
			teamData.next();
			Team team = new Team();
			team.setId(teamData.getInt("id"));
			team.setName(teamData.getString("name"));
			return team;
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return null;
    }

    public Team getTeamById(int teamId) throws DBException {
        String sql = "SELECT * FROM teams WHERE id = ?";
        try (Connection con = getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, teamId);
            ResultSet teamData = statement.executeQuery();
            teamData.next();
            Team team = new Team();
            team.setId(teamData.getInt("id"));
            team.setName(teamData.getString("name"));
            return team;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Team> findAllTeams() throws DBException {

        List<Team> teams = new ArrayList<>();
        try (Connection con = getConnection();
             Statement statement = con.createStatement();
             ResultSet set = statement.executeQuery("SELECT * FROM teams");) {
            while (set.next()) {
                Team team = new Team();
                team.setId(set.getInt("id"));
                team.setName(set.getString("name"));
                teams.add(team);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teams;
    }

    public boolean insertTeam(Team team) throws DBException {
        String sql = "INSERT INTO teams (name) VALUES (?)";
        try (Connection con = getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, team.getName());
            statement.execute();
            Team igor = getTeam(team.getName());
            team.setId(igor.getId());
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean setTeamsForUser(User user, Team... teams) throws DBException {
        String sqlToAddUserTeams = "INSERT INTO users_teams (user_id, team_id) VALUES (?, ?)";
        try (Connection con = getConnection();) {
            con.setAutoCommit(false);
            try (PreparedStatement statement = con.prepareStatement(sqlToAddUserTeams)) {
                User currentUser = getUser(user.getLogin());
                for (Team team : teams) {
                    Team currentTeam = getTeam(team.getName());
                    statement.setInt(1, currentUser.getId());
                    statement.setInt(2, currentTeam.getId());
                    statement.execute();
                }
            } catch (SQLException e) {
                con.rollback();
                e.printStackTrace();
                throw new DBException(e.getMessage(), e.getCause());
            }
            con.commit();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public List<Team> getUserTeams(User user) throws DBException {
        List<Team> teamList = new ArrayList<>();
        User currentUser = getUser(user.getLogin());
        String sql = "SELECT id, name FROM teams JOIN users_teams ON team_id = id WHERE user_id = ?";
        try (Connection con = getConnection()) {
            PreparedStatement statement = con.prepareStatement(sql);
            statement.setInt(1, currentUser.getId());
            statement.execute();
            ResultSet set = statement.getResultSet();
            if (set == null) {
                return teamList;
            }
            while (set.next()) {
                Team team = new Team();
                team.setId(set.getInt("id"));
                team.setName(set.getString("name"));
                teamList.add(team);
            }
            return teamList;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return teamList;
    }

    public boolean deleteTeam(Team team) throws DBException {
        String sql = "DELETE FROM teams WHERE id = ? AND name = ?";
        Team currentTeam = getTeam(team.getName());
        try (Connection con = getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, currentTeam.getId());
            statement.setString(2, currentTeam.getName());
            statement.execute();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public boolean updateTeam(Team team) throws DBException {
        String sql = "UPDATE teams SET name = ? WHERE id = ?";
        try (Connection con = getConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, team.getName());
            statement.setInt(2, team.getId());
            statement.executeUpdate();
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return false;
    }

    public static void main(String[] args) throws DBException {
        User user = new User();
        user.setLogin("Игорь");
        user.setId(2);
        Team team = new Team();
        team.setName("qwer");
        team.setId(2);
        Team team1 = new Team();
        team1.setId(3);
        team1.setName("updated");

        System.out.println(new DBManager().insertUser(user));
    }

}
